##Generate PDF Solution

### 1. (Server) Use php module [dompdf](https://github.com/dompdf/dompdf)

We can use html as the pdf template, Easily package as drupal module.

Here is demo <https://hiopensource.com/pdf/htmlToImage.html>

Demo source <https://bitbucket.org/xuanxian/php-pdf-demo>

#####Support

* HTML
* CSS2
* external stylesheets
* SVG
* image

#####Requirements (drupal already contains all)

* PHP version 5.3.0 or higher
* DOM extension
* GD extension
* MBString extension
* php-font-lib
* php-svg-lib
 
#####Notice

* Only support css2 and part css3

-------
### 2. (Server) Use [wkhtmltopdf](https://wkhtmltopdf.org)

We use node, php or other language to package the wkhtmltopdf command. 

#####Support

* HTML
* CSS
* external stylesheets
* Javascript
* SVG
* image

 
#####Requirements

* Install wkhtmltopdf to server.

---

### 3. (Browser) Use [pdfmake](http://pdfmake.org/)

Only need use JS handle pdf generate on client 

#####Support

* image

#####Notice

* Can't save pdf to server
* The layout is difficult


### 4. (Browser) Use [jsPDF](https://parall.ax/products/jspdf)

Only need use JS handle pdf generate on client 

#####Support

* image
* part html
* part css

#####Notice

* Can't save pdf to server
* The layout is difficult


### 4. (Browser) Use [rasterizeHTML](https://cburgmer.github.io/rasterizeHTML.js/) or [html2canvas](https://github.com/niklasvh/html2canvas) to convert HTML Dom to image, then use [jsPDF](https://parall.ax/products/jspdf) or [pdfmake](http://pdfmake.org/) generate pdf.

Only need use JS handle pdf generate on client 

#####Support

* html
* css
* image
* svg

#####Notice

* Can't save pdf to server
* Can't select and copy the text on the generated pdf
* Convert html to image, Contrl the size is difficult.
* Convert sub dom to image have problem, the css will be lose.