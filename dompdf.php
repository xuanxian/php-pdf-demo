<?php
 date_default_timezone_set('America/Los_Angeles');


require_once __DIR__ . '/dompdf/autoload.inc.php';
 
 
use Dompdf\Dompdf;

$HTML = file_get_contents('./index.html', FILE_USE_INCLUDE_PATH);

// instantiate and use the dompdf class
$dompdf = new Dompdf();
$dompdf->loadHtml($HTML);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'landscape');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
$dompdf->stream("domToPDFDemo.pdf", array("Attachment" => false));